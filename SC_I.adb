		-- **** Preprocessor ****

with Text_IO; use Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with auth; 
--with power_mode;
--with statue;
		-- **** sc-I's procedure ****

procedure SC_I is


	us: Integer; -- User_input
	actual_power: Integer; -- Will keep SC-I power state (1/0)

		
		-- **** authentification's procedure ****
	procedure authentification is
		use auth; -- Use package auth 
		id : String:= master_id;
		psswd: String := master_psswd;
		tries: Integer:= 5;
	begin
		Put_Line("Authentification Required.");
		
		loop

			Put_Line ("Remaining attempts: " & Integer'Image(tries));
			Put_Line(""); 
	
			Put ("ID: ");
			Get (id);
	
			Put_Line ("");
			Put ("Password: ");
			Get (psswd);
				
			if id = master_id and psswd = master_psswd then
				Put_Line ("Welcome " & master_name);
				exit;
	
			else
				Put_Line ("Wrong password or id");
				tries := tries -1;

			end if;
		end loop;
	end authentification;

	procedure power_mode (actual_power: in out Integer) is
		confirm : Integer; -- Will be use to confirm turning off SC-I
		--execute: String:= "y";
		--cancel: String:= "n";
		on: String:= "SC-I is actually on";
		off: String:= "SC-I is actually off";
	begin


		-- **** Turn on SC-I ****


		if actual_power = 0 then
			actual_power := 1;
			Put_Line ("Turning on SC-I...");
			Put_Line ("Operation executed successfully.");
			Put_Line (on);
	
	
			-- **** Turn off SC-I ****
		elsif actual_power = 1 then
			Put_Line ("");
			Put ("Are you to turn off SC-I?: ");
			Get (confirm);
			
			if confirm = 1 then
				actual_power := 0;
				Put_Line ("Turning off SC-I... ");
				Put_Line ("Operation executed succesfully.");
				Put_Line (off);
			elsif confirm = 0 then
				Put_Line ("Operation cancelled");
				Put_Line (on);
			end if;
		end if;
	end power_mode;

		procedure manual is
		begin
			Put_Line ("The available option are the following: ");	
			Put_Line ("(1). Print SC-I manual.");
			Put_Line ("(2). Set SC-I on/off.");
			Put_Line ("(3). Output state off all SC-I's procesus.");
			Put_Line ("(4). Exit SC-I init system.");
		end manual;
		
		procedure statue (actual_power: Integer) is

begin
	if actual_power = 1 then
		Put_Line ("SC-I power mode: on");

	elsif actual_power= 0 then
		Put_Line ("SC-I power mode: off");
	end if;
end statue;

begin
	
	authentification;
	manual;
	loop
		Get (us);
		if us = 1 then
			manual ;
		elsif us = 2 then
			power_mode(actual_power);
		elsif us = 3 then
			statue(actual_power);
		elsif us = 4 then
			exit;
		end if;
	end loop;
end SC_I;

